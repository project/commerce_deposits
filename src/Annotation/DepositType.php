<?php

namespace Drupal\commerce_deposits\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Deposit type item annotation object.
 *
 * @see \Drupal\commerce_deposits\Plugin\DepositTypeManager
 * @see plugin_api
 *
 * @Annotation
 */
class DepositType extends Plugin { 


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
