<?php

namespace Drupal\commerce_deposits;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\user\UserInterface;

/**
 * Defines the interface for payment gateway storage.
 */
interface CommerceDepositStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Loads all eligible payment gateways for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\commerce_deposits\Entity\CommerceDepositInterface[]
   *   The commerce deposits.
   */
  public function loadMultipleForOrder(OrderInterface $order);

}
