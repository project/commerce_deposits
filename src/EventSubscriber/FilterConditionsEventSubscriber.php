<?php

namespace Drupal\commerce_deposits\EventSubscriber;

use Drupal\commerce\Event\FilterConditionsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FilterConditionsEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce.filter_conditions' => 'onFilterConditions',
    ];
    return $events;
  }

  /**
   * Removes the payment gateway condition on payment gateways.
   *
   * @param \Drupal\commerce\Event\FilterConditionsEvent $event
   *   The event.
   */
  public function onFilterConditions(FilterConditionsEvent $event) {
    if ($event->getParentEntityTypeId() == 'commerce_deposit') {
      $definitions = $event->getDefinitions();
      unset($definitions['commerce_deposit']);
      $event->setDefinitions($definitions);
    }
  }

}
