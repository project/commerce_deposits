<?php

namespace Drupal\commerce_deposits\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;



/**
 * Manages discovery and instantiation of payment type plugins.
 *
 * @see \Drupal\commerce_deposits\Annotation\DepositType
 * @see plugin_api
 */
class DepositTypeManager extends DefaultPluginManager {


  /**
   * Constructs a new DepositTypeManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/DepositType', $namespaces, $module_handler, 'Drupal\commerce_deposits\Plugin\DepositTypeInterface', 'Drupal\commerce_deposits\Annotation\DepositType');

    $this->alterInfo('commerce_deposits_deposit_type_info');
    $this->setCacheBackend($cache_backend, 'commerce_deposits_deposit_type_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    foreach (['id', 'label'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new PluginException(sprintf('The deposit type %s must define the %s property.', $plugin_id, $required_property));
      }
    }
  }

  /**
  * Calculates the required deposit for a given order.
  */
  public function calculateDeposit(OrderInterface $order) {
    $entityTypeManager = \Drupal::service('entity_type.manager')->getStorage('commerce_deposit');
    $depositTotal = new Price('0', $order->getStore()->getDefaultCurrencyCode());
    $deposits = $entityTypeManager->loadMultipleForOrder($order);
    
    foreach($deposits as $deposit) {
      $plugin = $deposit->getPlugin();
      
      if ($depositAmount = $plugin->calculateDeposit($order)) {
        return $depositAmount;
      }
      
    }
    
    return $depositTotal;
  }

}
