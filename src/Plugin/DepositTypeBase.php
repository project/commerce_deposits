<?php

namespace Drupal\commerce_deposits\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for Deposit type plugins.
 */
abstract class DepositTypeBase extends PluginBase implements DepositTypeInterface {

  /**
  * {@inheritdoc}
  */
  public function calculateDeposit(OrderInterface $order) {
  	return FALSE;
  }

  /**
  * {@inheritdoc}
  */
  public function getSummary() {
  	return '';
  }

  /**
  * {@inheritdoc}
  */
  public function getStatus() {
    return FALSE;
  }

  /**
  * {@inheritdoc}
  */
  public function buildConfigurationForm(&$form, FormStateInterface $form_state) { }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}
  
  /**
  * {@inheritdoc}
  */
  public function submitConfigurationForm(&$form, FormStateInterface $form_state) { }

   /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }
}
