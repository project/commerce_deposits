<?php

namespace Drupal\commerce_deposits\Plugin\DepositType;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_deposits\Plugin\DepositTypeBase;
use Drupal\commerce_deposits\Plugin\DepositTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
* @DepositType(
*   id = "fixed_deposit",
*   label = @Translation("Fixed Deposit")
* )
*/
class FixedDeposit extends DepositTypeBase implements DepositTypeInterface { 


	/**
	* {@inheritdoc}
	*/
	public function calculateDeposit(OrderInterface $order) { 
		
		if($price = $this->configuration ) {
			
			$depositPrice = new Price( $price['number'], $price['currency_code'] );
			return $depositPrice;
		}
		return FALSE;
	}

	/**
	* {@inheritdoc}
	*/
	public function getSummary() {
		if ($amount = $this->configuration) {
			$fixedPrice = new Price($amount['number'], $amount['currency_code']);
			return t('Require a fixed deposit of @deposit_amount', ['@deposit_amount' => $fixedPrice->__toString() ]);	
		}
		return t('No fixed deposit configured.');
	}

	/**
	* {@inheritdoc}
	*/
	public function getStatus() {
		return $this->config->get('enabled');
	}

	/**
	* {@inheritdoc}
	*/
	public function buildConfigurationForm(&$form, FormStateInterface $form_state) { 
		
		$price = new Price( $this->configuration['number'], $this->configuration['currency_code'] );
		$form['fixed_rate'] = [
	      '#type' => 'commerce_price',
	      '#title' => t('Fixed Amount'),
	      '#default_value' => $this->configuration,
	      '#required' => TRUE,
	      '#tree' => TRUE
	    ];
	    return $form;
	}

	/**
	* {@inheritdoc}
	*/
	public function submitConfigurationForm(&$form, FormStateInterface $form_state) { 
		$values = $form_state->getValues();
		
		parent::submitConfigurationForm($form, $form_state);

	    if (!$form_state->getErrors()) {
	      $values = $form_state->getValue($form['#parents']);
	      $this->configuration = array_shift($values);
	    }
	}

}