<?php

namespace Drupal\commerce_deposits\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Defines an interface for Deposit type plugins.
 */
interface DepositTypeInterface extends PluginInspectionInterface {

	/**
	* Add get/set methods for your plugin type here.
	* @param Order $order 
	* @return Price|bool $price
	*/
	public function calculateDeposit(OrderInterface $order);

	/**
	* returns the summary message for this plugin to display on the configuration pages.
	* @return string
	*/
	public function getSummary();

	/**
	* returns the status for the plugin.
	*/
	public function getStatus();

	/**
	* Returns the configuration form for a Deposit type plugin.
	*/
	public function buildConfigurationForm(&$form, FormStateInterface $form_state);

	/**
  	 * {@inheritdoc}
   	*/
  	public function validateConfigurationForm(array &$form, FormStateInterface $form_state);

	/**
	* Submits the configuration form for a Deposit type plugin.
	*/
	public function submitConfigurationForm(&$form, FormStateInterface $form_state);

	/**
	* Returns the configuration for this plugin.
	*/
  	public function getConfiguration();
}
