<?php 

/**
* @file - This contains the Service that handles calculations for a 
* order's deposit.
*/

namespace Drupal\commerce_deposits;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;

class DepositManager { 

	public function calculateDeposits(OrderInterface $order) {
		$entityTypeManager = \Drupal::service('entity_type.manager')->getStorage('commerce_deposit');
		$depositTotal = new Price('0', $order->getStore()->getDefaultCurrencyCode());
		$deposits = $entityTypeManager->loadMultipleForOrder($order);
		foreach($deposits as $deposit) {
			$plugin = $deposit->getPlugin();
			if ($depositAmount = $plugin->calculateDeposit($order)) {
				$depositTotal->add($depositAmount);	
			}
		}
		return $depositTotal;
	}

}