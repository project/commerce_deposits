<?php

namespace Drupal\commerce_deposits\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\commerce\CommerceSinglePluginCollection;

/**
 * Defines the Deposit entity.
 *
 * @ConfigEntityType(
 *   id = "commerce_deposit",
 *   label = @Translation("Deposit"),
 *   label_collection = @Translation("Deposits"),
 *   label_singular = @Translation("deposit"),
 *   label_plural = @Translation("deposits"),
 *   label_count = @PluralTranslation(
 *     singular = "@count deposit",
 *     plural = "@count deposits",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\commerce_deposits\CommerceDepositStorage",   
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_deposits\CommerceDepositListBuilder",
 *     "form" = {
 *       "add" = "Drupal\commerce_deposits\Form\CommerceDepositForm",
 *       "edit" = "Drupal\commerce_deposits\Form\CommerceDepositForm",
 *       "delete" = "Drupal\commerce_deposits\Form\CommerceDepositDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_deposits\CommerceDepositHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "commerce_deposit",
 *   admin_permission = "administer commerce_deposits",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *     "status" = "status,"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "status",
 *     "plugin",
 *     "configuration",
 *     "conditions",
 *     "conditionOperator",
 *   },
 *   links = {
 *     "canonical" = "/admin/commerce/config/deposits/commerce_deposit/{commerce_deposit}",
 *     "add-form" = "/admin/commerce/config/deposits/commerce_deposit/add",
 *     "edit-form" = "/admin/commerce/config/deposits/commerce_deposit/{commerce_deposit}/edit",
 *     "delete-form" = "/admin/commerce/config/deposits/commerce_deposit/{commerce_deposit}/delete",
 *     "collection" = "/admin/commerce/config/deposits/commerce_deposit"
 *   }
 * )
 */
class CommerceDeposit extends ConfigEntityBase implements CommerceDepositInterface {

  /**
   * The Deposit ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Deposit label.
   *
   * @var string
   */
  protected $label;

  /**
  * The deposit weight.
  * 
  * @var int
  */
  protected $weight;

  /**
  * The plugin ID.
  *
  * @var string
  */
  protected $plugin;

  /**
  * The plugin configuration.
  *
  * @var array
  */
  protected $configuration = [];

  /**
  * The conditions.
  *
  * @var array
  */
  protected $conditions = [];

  /**
  * The condition operator.
  *
  * @var string.
  */
  protected $conditionOperator = 'AND';

  /**
  * The plugin collection that holds the deposit plugin.
  *
  * @var \Drual\commerce\CommerceSinglePluginCollection
  */
  protected $pluginCollection;

  /**
  * {@inheritdoc}
  */
  public function getWeight() {
    return $this->weight;
  }

  /**
  * {@inheritdoc}
  */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $weight;
  }

  /**
  * {@inheritdoc}
  */
  public function getPlugin() {
    return $this->getPluginCollection()->get($this->plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->plugin;
  }

  /**
  * {@inheritdoc}
  */
  public function setPluginId($plugin_id) {
    $this->plugin = $plugin_id;
    $this->configuration = [];
    $this->pluginCollection = NULL;
    return $this;
  }

  /**
  * {@inheritdoc}
  */
  public function getPluginConfiguration() {
    return $this->configuration;
  }

    /**
   * {@inheritdoc}
   */
  public function setPluginConfiguration(array $configuration) {
    $this->configuration = $configuration;
    $this->pluginCollection = NULL;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'configuration' => $this->getPluginCollection(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() {
    $plugin_manager = \Drupal::service('plugin.manager.commerce_condition');
    $conditions = [];
    foreach ($this->conditions as $condition) {
      $condition = $plugin_manager->createInstance($condition['plugin'], $condition['configuration']);
      if ($condition instanceof ParentEntityAwareInterface) {
        $condition->setParentEntity($this);
      }
      $conditions[] = $condition;
    }
    return $conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionOperator() {
    return $this->conditionOperator;
  }

  /**
   * {@inheritdoc}
   */
  public function setConditionOperator($condition_operator) {
    $this->conditionOperator = $condition_operator;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(OrderInterface $order) {
    $conditions = $this->getConditions();
    if (!$conditions) {
      // Deposits without conditions always apply.
      return TRUE;
    }
    $order_conditions = array_filter($conditions, function ($condition) {
      /** @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $condition */
      return $condition->getEntityTypeId() == 'commerce_order';
    });
    $order_conditions = new ConditionGroup($order_conditions, $this->getConditionOperator());

    return $order_conditions->evaluate($order);
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value) {
    // Invoke the setters to clear related properties.
    if ($property_name == 'plugin') {
      $this->setPluginId($value);
    }
    elseif ($property_name == 'configuration') {
      $this->setPluginConfiguration($value);
    }
    else {
      return parent::set($property_name, $value);
    }
  }

  /**
   * Gets the plugin collection that holds the payment gateway plugin.
   *
   * Ensures the plugin collection is initialized before returning it.
   *
   * @return \Drupal\commerce\CommerceSinglePluginCollection
   *   The plugin collection.
   */
  protected function getPluginCollection() {
    if (!$this->pluginCollection) {
      $plugin_manager = \Drupal::service('plugin.manager.deposit_type');
      $this->pluginCollection = new CommerceSinglePluginCollection($plugin_manager, $this->plugin, $this->configuration, $this->id);
    }
    return $this->pluginCollection;
  }
  
}
