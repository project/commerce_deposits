<?php 

/**
* @file - Altered order class that allows handling for deposit logic.
*/
namespace Drupal\commerce_deposits\Entity;

use Drupal\commerce_order\Entity\Order as BaseOrder;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Order extends BaseOrder implements OrderInterface { 

	protected $depositManager;

	/**
     * Constructs a new Order endity.
     */
	 public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = array()) {
	 	parent::__construct($values, $entity_type, $bundle, $translations);
	    $this->depositManager = \Drupal::service('plugin.manager.deposit_type');
	 }

	/**
	* {@inheritdoc}
	*/
	public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
		$fields = parent::baseFieldDefinitions($entity_type);
		
		$fields['deposit_paid'] = BaseFieldDefinition::create('commerce_price')
	      ->setLabel(t('Total paid'))
	      ->setDescription(t('The total deposit paid price of the order.'))
	      ->setDisplayConfigurable('form', FALSE)
	      ->setDisplayConfigurable('view', TRUE);

		return $fields;
	}


	/**
	* {@inheritdoc} 
	*/
	public function getTotalDepositPrice() { 
	  return $this->depositManager->calculateDeposit($this);
	}

	/**
	* {@inheritdoc}
	*/
	public function getTotalDepositPaid() { 
		if (!$this->get('deposit_paid')->isEmpty()) {
	      return $this->get('deposit_paid')->first()->toPrice();
	    }
	    elseif ($total_price = $this->getTotalDepositPrice()) {
	      // Provide a default without storing it, to avoid having to update
	      // the field if the order currency changes before the order is placed.
	      return new Price('0', $total_price->getCurrencyCode());
	    }
	}

	/**
	* {@inheritdoc}
	*/
	public function getDepositBalance() { 
	  if ($total_price = $this->getTotalDepositPrice()) {
      	return $total_price->subtract($this->getTotalDepositPaid());
      }
	}

	/**
    * Returns if the deposit has been paid.
    * @return bool
    */
    public function isDepositPaid() {
	  $total_price = $this->getTotalDepositPrice();
	  if (!$total_price) {
	    return FALSE;
	  }

	  $balance = $this->getDepositBalance();
	  // Free orders are considered fully paid once they have been placed.
	  if ($total_price->isZero()) {
	     return $this->getState()->getId() != 'draft';
	  }
	  else {
	    return $balance->isNegative() || $balance->isZero();
	  }
	}

	/**
   * {@inheritdoc}
   */
	public function getTotalPrice() {
	  if (!$this->isDepositPaid()) {
	  	return $this->getDepositBalance();
	  }
	  parent::getTotalPrice();
	}

}