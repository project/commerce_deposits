<?php 

/**
* @file - Contains the extended Commerce Order interface for handling deposits.
*/
namespace Drupal\commerce_deposits\Entity;

use Drupal\commerce_order\Entity\OrderInterface as BaseOrderInterface;


interface OrderInterface extends BaseOrderInterface { 

	/**
	* Calculates the total deposit price for a given order.
	* @return \Drupal\commerce_price\Price
	*/
	public function getTotalDepositPrice();

	/**
	* Calculates the total deposit that has been paid for a order.
	* @return \Drupal\commerce_price\Price
	*/
	public function getTotalDepositPaid();

	/**
	* Returns the current balance for the deposit on a order.
	* @return \Drupal\commerce_price\Price
	*/
	public function getDepositBalance();

	/**
	* Returns if the deposit has been paid or not.
	* @return bool
	*/
	public function isDepositPaid();
}