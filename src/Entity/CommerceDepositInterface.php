<?php

namespace Drupal\commerce_deposits\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Deposit entities.
 */
interface CommerceDepositInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
