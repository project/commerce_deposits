<?php

namespace Drupal\commerce_deposits\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_deposits\Plugin\DepositTypeManager;

/**
 * Mail Tracker.
 */
class CommerceDepositTypeList extends ControllerBase {

  /**
   * The payment gateway plugin manager.
   *
   * @var \Drupal\commerce_deposits\DepositTypeManager
   */
  protected $pluginManager;

  /**
   * Constructs a new PaymentGatewayForm object.
   *
   * @param \Drupal\commerce_deposits\DepositTypeManager $plugin_manager
   *   The payment gateway plugin manager.
   */
  public function __construct(DepositTypeManager $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.deposit_type')
    );
  }

  /**
   * Mail tracker controller callback.
   */
  public function depositPluginList() { 
    $header = [
      t('Deposit Type'),
      t('Summary'),
      t('Status'),
      t('Operations'),
    ];

    $rows = [];
    
    $plugins = array_column($this->pluginManager->getDefinitions(), 'label', 'id');
    asort($plugins);
    
    
    foreach($plugins as $id => $pluginDefinition) {
      
      $plugin = $this->pluginManager->createInstance($id, []);
      $row = [
        $pluginDefinition,
        $plugin->getSummary(),
        $plugin->getStatus() ? t('Active') : t('Disabled'),
        Link::createFromRoute(t('Edit'), 'commerce_payment.plugin_configuration', ['plugin_id' => $id])
      ];

      $rows[] = $row;
    }


    return [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('There are currently no Deposit methods.')
    ];
  }

}
