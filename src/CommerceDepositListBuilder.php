<?php

namespace Drupal\commerce_deposits;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Deposit entities.
 */
class CommerceDepositListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Deposit');
    $header['summary'] = $this->t('Summary');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['summary'] = $entity->getPlugin()->getSummary();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}
