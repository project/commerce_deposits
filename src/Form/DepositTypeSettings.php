<?php 

/**
* @file - Contains the form that allows for displaying the various 
* plugin configurations.
*/
namespace Drupal\commerce_deposits\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_deposits\Plugin\DepositTypeManager;




class DepositTypeSettings extends FormBase {

  protected $pluginManager;

  /**
   * Constructs a new DepositTypeSettingsForm.
   *
   * @param \Drupal\commerce_deposits\DepositTypeManager $plugin_manager
   *   The Deposit type plugin manager.
   */
  public function __construct(DepositTypeManager $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.deposit_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'deposit_type_settings';
  }

  /**
  * {@inheritdoc}
  */
  public function buildForm(array $form, FormStateInterface $form_state, $plugin_id = NULL) { 
  	if ($plugin =  $this->pluginManager->createInstance($plugin_id, [])) {
      
      $form['plugin_id'] = [
        '#type' => 'value',
        '#value' => $plugin_id,
      ];
      
      $plugin->buildDepositConfigurationForm($form, $form_state);

      $form['configuration'] = [
        '#type' => 'commerce_plugin_configuration',
        '#plugin_type' => 'depoist_type',
        '#plugin_id' => $plugin,
        '#default_value' => $plugin_configuration,
      ];
      $form['conditions'] = [
        '#type' => 'commerce_conditions',
        '#title' => $this->t('Conditions'),
        '#parent_entity_type' => 'commerce_order',
        '#entity_types' => ['commerce_order'],
        '#default_value' => $gateway->get('conditions'),
      ];
      $form['conditionOperator'] = [
        '#type' => 'radios',
        '#title' => $this->t('Condition operator'),
        '#title_display' => 'invisible',
        '#options' => [
          'AND' => $this->t('All conditions must pass'),
          'OR' => $this->t('Only one condition must pass'),
        ],
        '#default_value' => $gateway->getConditionOperator(),
      ];

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Save')
      ];
    }
    
  	return $form;
  }

  /**
  * {@inheritdoc}
  */
  public function validateForm(array &$form, FormStateInterface $form_state) { 
    $plugin = $this->pluginManager->createInstance($form_state->getValue('plugin_id'), []);
    $plugin->validateDepositConfigurationForm($form, $form_state);
  }

  /**
  * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) { 
    $plugin = $this->pluginManager->createInstance($form_state->getValue('plugin_id'), []);
    $plugin->submitDepositConfigurationForm($form, $form_state);
  }


}