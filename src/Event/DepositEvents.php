<?php

namespace Drupal\commerce_deposits\Event;

final class DepositEvents {

  /**
   * Name of the event fired when payment gateways are loaded for an order.
   *
   * @Event
   *
   * @see \Drupal\commerce_deposits\Event\FilterDepositsEvent
   */
  const FILTER_DEPOSITS = 'commerce_deposits.filter_deposits';

}
