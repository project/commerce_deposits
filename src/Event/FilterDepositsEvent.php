<?php

namespace Drupal\commerce_deposits\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the event for filtering the available payment gateways.
 *
 * @see \Drupal\commerce_deposits\Event\DepositEvents
 */
class FilterDepositsEvent extends Event {

  /**
   * The deposits.
   *
   * @var \Drupal\commerce_deposits\Entity\CommerceDepositInterface[]
   */
  protected $deposits;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Constructs a new FilterDepositsEvent object.
   *
   * @param \Drupal\commerce_deposits\Entity\CommerceDepositInterface[] $deposits
   *   The payment gateways.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function __construct(array $deposits, OrderInterface $order) {
    $this->deposits = $deposits;
    $this->order = $order;
  }

  /**
   * Gets the deposits.
   *
   * @return \Drupal\commerce_deposits\Entity\CommerceDepositInterface[]
   *   The deposits.
   */
  public function getDeposits() {
    return $this->deposits;
  }

  /**
   * Sets the deposits.
   *
   * @param \Drupal\commerce_deposits\Entity\CommerceDepositInterface[] $deposits
   *   The deposits.
   *
   * @return $this
   */
  public function setDeposits(array $deposits) {
    $this->deposits = $deposits;
    return $this;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder() {
    return $this->order;
  }

}
